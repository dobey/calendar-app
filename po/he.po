# Hebrew translation for ubuntu-calendar-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-calendar-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-calendar-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-07 08:29+0000\n"
"PO-Revision-Date: 2020-09-05 15:34+0000\n"
"Last-Translator: tamer dab <dabsantamer@yahoo.com>\n"
"Language-Team: Hebrew <https://translate.ubports.com/projects/ubports/"
"calendar-app/he/>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 3.11.3\n"
"X-Launchpad-Export-Date: 2017-04-05 07:14+0000\n"

#: ../qml/TimeLineBase.qml:50 ../qml/AllDayEventComponent.qml:89
msgid "New event"
msgstr "אירוע חדש"

#: ../qml/CalendarChoicePopup.qml:44 ../qml/EventActions.qml:54
msgid "Calendars"
msgstr "יומנים"

#: ../qml/CalendarChoicePopup.qml:46 ../qml/SettingsPage.qml:51
msgid "Back"
msgstr "אחורה"

#. TRANSLATORS: Please translate this string  to 15 characters only.
#. Currently ,there is no way we can increase width of action menu currently.
#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Sync"
msgstr "סנכרון"

#: ../qml/CalendarChoicePopup.qml:58 ../qml/EventActions.qml:39
msgid "Syncing"
msgstr "בסנכרון"

#: ../qml/CalendarChoicePopup.qml:83
msgid "Add online Calendar"
msgstr "הוספת לוח שנה מקוון"

#: ../qml/CalendarChoicePopup.qml:184
msgid "Unable to deselect"
msgstr "לא ניתן לבטל בחירה"

#: ../qml/CalendarChoicePopup.qml:185
msgid ""
"In order to create new events you must have at least one writable calendar "
"selected"
msgstr "על מנת ליצור אירוע חדש חייב להיות בחור לפחות לוח שנה אחד שניתן לכתיבה"

#: ../qml/CalendarChoicePopup.qml:187 ../qml/RemindersPage.qml:80
msgid "Ok"
msgstr "אישור"

#. TRANSLATORS: the first argument (%1) refers to a start time for an event,
#. while the second one (%2) refers to the end time
#: ../qml/EventBubble.qml:139
msgid "%1 - %2"
msgstr "%1 - %2"

#: ../qml/RemindersModel.qml:31 ../qml/RemindersModel.qml:99
msgid "No Reminder"
msgstr "ללא תזכורת"

#. TRANSLATORS: this refers to when a reminder should be shown as a notification
#. in the indicators. "On Event" means that it will be shown right at the time
#. the event starts, not any time before
#: ../qml/RemindersModel.qml:34 ../qml/RemindersModel.qml:103
msgid "On Event"
msgstr "בזמן האירוע"

#: ../qml/RemindersModel.qml:43
msgid "%1 week"
msgid_plural "%1 weeks"
msgstr[0] "%1 שבוע"
msgstr[1] "%1 שבועות"

#: ../qml/RemindersModel.qml:54
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 יום"
msgstr[1] "%1 ימים"

#: ../qml/RemindersModel.qml:65
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "שעה %1"
msgstr[1] "%1 שעות"

#: ../qml/RemindersModel.qml:74
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "דקה %1"
msgstr[1] "%1 דקות"

#: ../qml/RemindersModel.qml:104
msgid "5 minutes"
msgstr "5 דקות"

#: ../qml/RemindersModel.qml:105
msgid "10 minutes"
msgstr "10 דקות"

#: ../qml/RemindersModel.qml:106
msgid "15 minutes"
msgstr "רבע שעה"

#: ../qml/RemindersModel.qml:107
msgid "30 minutes"
msgstr "חצי שעה"

#: ../qml/RemindersModel.qml:108
msgid "1 hour"
msgstr "שעה 1"

#: ../qml/RemindersModel.qml:109
msgid "2 hours"
msgstr "שעתיים"

#: ../qml/RemindersModel.qml:110
msgid "1 day"
msgstr "יום 1"

#: ../qml/RemindersModel.qml:111
msgid "2 days"
msgstr "יומיים"

#: ../qml/RemindersModel.qml:112
msgid "1 week"
msgstr "שבוע 1"

#: ../qml/RemindersModel.qml:113
msgid "2 weeks"
msgstr "שבועיים"

#: ../qml/RemindersModel.qml:114
msgid "Custom"
msgstr "התאמה אישית"

#: ../qml/AgendaView.qml:50 ../qml/calendar.qml:333 ../qml/calendar.qml:514
msgid "Agenda"
msgstr "סדר יום"

#: ../qml/AgendaView.qml:95
msgid "You have no calendars enabled"
msgstr "אין לוחות שנה פעילים"

#: ../qml/AgendaView.qml:95
msgid "No upcoming events"
msgstr "אין אירועים בזמן הקרוב"

#: ../qml/AgendaView.qml:107
msgid "Enable calendars"
msgstr "הפעלת לוחות שנה"

#: ../qml/AgendaView.qml:199
msgid "no event name set"
msgstr "לא הוגדר שם האירוע"

#: ../qml/AgendaView.qml:201
msgid "no location"
msgstr "אין מיקום"

#: ../qml/LimitLabelModel.qml:25
msgid "Never"
msgstr "לעולם לא"

#: ../qml/LimitLabelModel.qml:26
msgid "After X Occurrence"
msgstr "לאחר מופע מסוים"

#: ../qml/LimitLabelModel.qml:27
msgid "After Date"
msgstr "לאחר התאריך"

#: ../qml/NewEventBottomEdge.qml:54 ../qml/NewEvent.qml:382
msgid "New Event"
msgstr "אירוע חדש"

#. TRANSLATORS: Please keep the translation of this string to a max of
#. 5 characters as the week view where it is shown has limited space.
#: ../qml/AllDayEventComponent.qml:148
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "%1 אירוע"
msgstr[1] "%1 אירועים"

#. TRANSLATORS: the argument refers to the number of all day events
#: ../qml/AllDayEventComponent.qml:152
msgid "%1 all day event"
msgid_plural "%1 all day events"
msgstr[0] "%1 אירוע על פני כל היום"
msgstr[1] "%1 אירועים על פני כל היום"

#: ../qml/EditEventConfirmationDialog.qml:29 ../qml/NewEvent.qml:382
msgid "Edit Event"
msgstr "עריכת אירוע"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/EditEventConfirmationDialog.qml:32
msgid "Edit only this event \"%1\", or all events in the series?"
msgstr "עריכה של האירוע הנוכחי \"%1\" , או את כל האירועים בסדרה?"

#: ../qml/EditEventConfirmationDialog.qml:35
msgid "Edit series"
msgstr "לערוך את הסדרה"

#: ../qml/EditEventConfirmationDialog.qml:44
msgid "Edit this"
msgstr "לערוך את המופע"

#: ../qml/EditEventConfirmationDialog.qml:53
#: ../qml/DeleteConfirmationDialog.qml:60 ../qml/RemindersPage.qml:72
#: ../qml/NewEvent.qml:387 ../qml/OnlineAccountsHelper.qml:73
#: ../qml/ColorPickerDialog.qml:55
msgid "Cancel"
msgstr "בטול"

#: ../qml/ContactChoicePopup.qml:37
msgid "No contact"
msgstr "אין איש קשר"

#: ../qml/ContactChoicePopup.qml:96
msgid "Search contact"
msgstr "חיפוש איש קשר"

#: ../qml/DeleteConfirmationDialog.qml:31
msgid "Delete Recurring Event"
msgstr "מחיקת אירועים חוזרים"

#: ../qml/DeleteConfirmationDialog.qml:32
msgid "Delete Event"
msgstr "מחיקת אירוע"

#. TRANSLATORS: argument (%1) refers to an event name.
#: ../qml/DeleteConfirmationDialog.qml:36
msgid "Delete only this event \"%1\", or all events in the series?"
msgstr "האם למחוק רק את האירוע \"%1\", או את כל האירועים בסדרה?"

#: ../qml/DeleteConfirmationDialog.qml:37
msgid "Are you sure you want to delete the event \"%1\"?"
msgstr "האם אתם בטוחים לגבי המחיקה של אירוע \"%1\"?"

#: ../qml/DeleteConfirmationDialog.qml:40
msgid "Delete series"
msgstr "מחיקת הסדרה"

#: ../qml/DeleteConfirmationDialog.qml:51
msgid "Delete this"
msgstr "למחוק את המופע"

#: ../qml/DeleteConfirmationDialog.qml:51 ../qml/NewEvent.qml:394
msgid "Delete"
msgstr "מחק"

#: ../qml/calendar.qml:74
msgid ""
"Calendar app accept four arguments: --starttime, --endtime, --newevent and --"
"eventid. They will be managed by system. See the source for a full comment "
"about them"
msgstr ""
"יישום היומן מקבל ארבעה ארגומנטים: --starttime, --endtime, --newevent and --"
"eventid. הם ינוהלו על ידי המערכת. ראה מקור להערה מלאה עליהם"

#: ../qml/calendar.qml:341 ../qml/calendar.qml:535
msgid "Day"
msgstr "יום"

#: ../qml/calendar.qml:349 ../qml/calendar.qml:556
msgid "Week"
msgstr "שבוע"

#: ../qml/calendar.qml:357 ../qml/calendar.qml:577
msgid "Month"
msgstr "חודש"

#: ../qml/calendar.qml:365 ../qml/calendar.qml:598
msgid "Year"
msgstr "שנה"

#: ../qml/calendar.qml:705 ../qml/TimeLineHeader.qml:66
#: ../qml/EventDetails.qml:173
msgid "All Day"
msgstr "כל היום"

#: ../qml/SettingsPage.qml:49 ../qml/EventActions.qml:66
msgid "Settings"
msgstr "הגדרות"

#: ../qml/SettingsPage.qml:72
msgid "Show week numbers"
msgstr "הצגת מספרי שבועות"

#: ../qml/SettingsPage.qml:90
msgid "Display Chinese calendar"
msgstr "הצג לוח שנה סיני"

#: ../qml/SettingsPage.qml:110
msgid "Business hours"
msgstr "שעות עבודה"

#: ../qml/SettingsPage.qml:211
msgid "Default reminder"
msgstr "התרעת ברירת מחדל"

#: ../qml/SettingsPage.qml:255
msgid "Default calendar"
msgstr "לוח שנה ברירת מחדל"

#: ../qml/RemindersPage.qml:62
msgid "Custom reminder"
msgstr "תזכורת מותאמת אישית"

#. TRANSLATORS: This is shown in the month view as "Wk" as a title
#. to indicate the week numbers. It should be a max of up to 3 characters.
#: ../qml/MonthComponent.qml:293
msgid "Wk"
msgstr "Wk"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the page to choose repetition
#. and as the header of the list item that shows the repetition
#. summary in the page that displays the event details
#: ../qml/EventRepetition.qml:40 ../qml/EventRepetition.qml:167
msgid "Repeat"
msgstr "חזרות"

#: ../qml/EventRepetition.qml:187
msgid "Repeats On:"
msgstr "חזרה בכל:"

#: ../qml/EventRepetition.qml:233
msgid "Interval of recurrence"
msgstr "מרווח של הישנות"

#: ../qml/EventRepetition.qml:258
msgid "Recurring event ends"
msgstr "סיום האירוע החוזר"

#. TRANSLATORS: this refers to how often a recurrent event repeats
#. and it is shown as the header of the option selector to choose
#. its repetition
#: ../qml/EventRepetition.qml:282 ../qml/NewEvent.qml:775
msgid "Repeats"
msgstr "חזרות"

#: ../qml/EventRepetition.qml:308
msgid "Date"
msgstr "תאריך"

#: ../qml/YearView.qml:57 ../qml/WeekView.qml:60 ../qml/MonthView.qml:50
#: ../qml/DayView.qml:76
msgid "Today"
msgstr "היום"

#: ../qml/YearView.qml:79
msgid "Year %1"
msgstr "שנת %1"

#: ../qml/NewEvent.qml:199
msgid "End time can't be before start time"
msgstr "מועד הסיום לא יכול להקדים את מועד ההתחלה"

#: ../qml/NewEvent.qml:412
msgid "Save"
msgstr "שמור"

#: ../qml/NewEvent.qml:423
msgid "Error"
msgstr "שגיאה"

#: ../qml/NewEvent.qml:425
msgid "OK"
msgstr "אישור"

#: ../qml/NewEvent.qml:487
msgid "From"
msgstr "מ"

#: ../qml/NewEvent.qml:503
msgid "To"
msgstr "ל"

#: ../qml/NewEvent.qml:530
msgid "All day event"
msgstr "אירוע של יום שלם"

#: ../qml/NewEvent.qml:553 ../qml/EventDetails.qml:37
msgid "Event Details"
msgstr "פרטי האירוע"

#: ../qml/NewEvent.qml:567
msgid "Event Name"
msgstr "שם האירוע"

#: ../qml/NewEvent.qml:585 ../qml/EventDetails.qml:437
msgid "Description"
msgstr "תיאור"

#: ../qml/NewEvent.qml:604
msgid "Location"
msgstr "מיקום"

#: ../qml/NewEvent.qml:619 ../qml/EventDetails.qml:348
#: com.ubuntu.calendar_calendar.desktop.in.h:1
msgid "Calendar"
msgstr "לוח שנה"

#: ../qml/NewEvent.qml:681
msgid "Guests"
msgstr "אורחים"

#: ../qml/NewEvent.qml:691
msgid "Add Guest"
msgstr "הוספת אורח"

#: ../qml/NewEvent.qml:797 ../qml/NewEvent.qml:814 ../qml/EventDetails.qml:464
msgid "Reminder"
msgstr "תזכורת"

#: ../qml/WeekView.qml:137 ../qml/MonthView.qml:76
msgid "%1 %2"
msgstr "%1 %2"

#: ../qml/WeekView.qml:144 ../qml/WeekView.qml:145
msgid "MMM"
msgstr "MMM"

#. TRANSLATORS: this is a time formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#. It's used in the header of the month and week views
#: ../qml/WeekView.qml:156 ../qml/MonthView.qml:81 ../qml/DayView.qml:126
msgid "MMMM yyyy"
msgstr "MMMM yyyy"

#. TRANSLATORS: W refers to Week, followed by the actual week number (%1)
#: ../qml/TimeLineHeader.qml:54
msgid "W%1"
msgstr "שב׳%1"

#: ../qml/EventDetails.qml:40
msgid "Edit"
msgstr "ערוך"

#: ../qml/EventDetails.qml:392
msgid "Attending"
msgstr "ישתתפו"

#: ../qml/EventDetails.qml:394
msgid "Not Attending"
msgstr "לא ישתתפו"

#: ../qml/EventDetails.qml:396
msgid "Maybe"
msgstr "אולי"

#: ../qml/EventDetails.qml:398
msgid "No Reply"
msgstr "אין תשובה"

#: ../qml/OnlineAccountsHelper.qml:39
msgid "Pick an account to create."
msgstr "בחר חשבון ליצירה."

#: ../qml/ColorPickerDialog.qml:25
msgid "Select Color"
msgstr "בחר צבע"

#: ../qml/RecurrenceLabelDefines.qml:23
msgid "Once"
msgstr "פעם אחת"

#: ../qml/RecurrenceLabelDefines.qml:24
msgid "Daily"
msgstr "מדי יום"

#: ../qml/RecurrenceLabelDefines.qml:25
msgid "On Weekdays"
msgstr "בימי השבוע"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday, Tuesday, Thursday"
#: ../qml/RecurrenceLabelDefines.qml:27
msgid "On %1, %2 ,%3"
msgstr "ב %1, %2, %3"

#. TRANSLATORS: The arguments refer to days of the week. E.g. "On Monday and Thursday"
#: ../qml/RecurrenceLabelDefines.qml:29
msgid "On %1 and %2"
msgstr "ב %1 וב %2"

#: ../qml/RecurrenceLabelDefines.qml:30
msgid "Weekly"
msgstr "מדי שבוע"

#: ../qml/RecurrenceLabelDefines.qml:31
msgid "Monthly"
msgstr "מדי חודש"

#: ../qml/RecurrenceLabelDefines.qml:32
msgid "Yearly"
msgstr "מדי שנה"

#. TRANSLATORS: the argument refers to multiple recurrence of event with count .
#. E.g. "Daily; 5 times."
#: ../qml/EventUtils.qml:75
msgid "%1; %2 time"
msgid_plural "%1; %2 times"
msgstr[0] "%1; %2 פעם אחת"
msgstr[1] "%1; %2 פעמים"

#. TRANSLATORS: the argument refers to recurrence until user selected date.
#. E.g. "Daily; until 12/12/2014."
#: ../qml/EventUtils.qml:79
msgid "%1; until %2"
msgstr "%1; עד %2"

#: ../qml/EventUtils.qml:93
msgid "; every %1 days"
msgstr "; כל %1 ימים"

#: ../qml/EventUtils.qml:95
msgid "; every %1 weeks"
msgstr "; כל %1 שבועות"

#: ../qml/EventUtils.qml:97
msgid "; every %1 months"
msgstr "; כל %1 חודשים"

#: ../qml/EventUtils.qml:99
msgid "; every %1 years"
msgstr "; כל %1 שנים"

#. TRANSLATORS: the argument refers to several different days of the week.
#. E.g. "Weekly on Mondays, Tuesdays"
#: ../qml/EventUtils.qml:125
msgid "Weekly on %1"
msgstr "שבועי ב %1"

#: com.ubuntu.calendar_calendar.desktop.in.h:2
msgid "A calendar for Ubuntu which syncs with online accounts."
msgstr "יומן לאובונטו שמסתנכרן עם חשבונות מקוונים."

#: com.ubuntu.calendar_calendar.desktop.in.h:3
msgid "calendar;event;day;week;year;appointment;meeting;"
msgstr "יומן;לוח שנה;אירוע;יום;שבוע;חודש;שנה;פגישה;מפגש;"

#~ msgid "Show lunar calendar"
#~ msgstr "הצג לוח שנה ירחי"
